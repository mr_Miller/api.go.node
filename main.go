package main

import (
	"api.go.node/config"
	"api.go.node/app"
)

func main() {
	configDB := config.GetConfig()
	appApi := &app.App{}
	appApi.Initialize(configDB)
	appApi.Run(":9090")
}
