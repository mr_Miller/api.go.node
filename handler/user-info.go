package handler

import (
	"github.com/jinzhu/gorm"
	"net/http"
	"github.com/gorilla/mux"
	"encoding/json"
	"api.go.node/app/model"
)

func getUserInfoOr404(db *gorm.DB, id string, w http.ResponseWriter, r *http.Request) *model.UserInfo {
	userInfo := &model.UserInfo{}
	if err := db.First(userInfo, "user_id = ?", id).Error; err != nil {
		HttpError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return userInfo
}

func UpdateUserInfo(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userInfo := getUserInfoOr404(db, vars["id"], w, r)
	if userInfo == nil {
		return
	}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&userInfo); err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	if err := db.Save(&userInfo).Error; err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
		return
	}
	RespondBool(w, true)
}
