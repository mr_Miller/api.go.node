package handler

import (
	"encoding/json"
	"math/big"
	"net/http"
)

// RespondData makes the response with payload as json format
func RespondData(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

// HttpError makes the error response with payload as json format
func HttpError(w http.ResponseWriter, code int, message string) {
	RespondData(w, code, map[string]string{"error": message})
}

func RespondString(w http.ResponseWriter, message string) {
	RespondData(w, http.StatusOK, map[string]string{"data": message})
}

func RespondBool(w http.ResponseWriter, flag bool) {
	RespondData(w, http.StatusOK, map[string]bool{"data": flag})
}

func RespondBalance(w http.ResponseWriter, balance *big.Float) {
	RespondData(w, http.StatusOK, map[string]*big.Float{"balance": balance})
}

//func respondInt(w http.ResponseWriter, code, number int) {
//	RespondData(w, code, map[string]int{"data": number})
//}

func StringToBigInt(s string) *big.Int {
	n := new(big.Int)
	n, ok := n.SetString(s, 10)
	if !ok {
		return nil
	}
	return n
}
