package handler

import (
	"api.go.node/app/model"
	"context"
	"encoding/json"
	"fmt"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ipfs/go-ipfs-api"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"math/big"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

type indexConf struct {
	DB   *gorm.DB
	IPFS *shell.Shell
}

func Master(db *gorm.DB, client *ethclient.Client, ipfs *shell.Shell, w http.ResponseWriter) {
	ctx := context.Background()
	header, err := client.HeaderByNumber(ctx, nil)
	if err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
		return
	}

	var settings = &model.Settings{}
	db.FirstOrCreate(settings, model.Settings{Params: "block_number_indexed"})
	indexConf := indexConf{DB: db, IPFS: ipfs}

	lastIndexBlock, _ := strconv.ParseInt(settings.Value, 10, 32)
	lastBlock, _ := strconv.ParseInt(header.Number.String(), 10, 32)
	indexNumber := 0
	for i := int(lastIndexBlock); i < int(lastBlock); i++ {
		blockInfo, err := client.BlockByNumber(ctx, big.NewInt(int64(i)))
		if err != nil {
			fmt.Println(err.Error() + ". Can't get latest block!!!")
			return
		}
		if blockInfo.Transactions().Len() > 0 {
			for _, v := range blockInfo.Transactions() {
				data := string(v.Data())
				transaction := &model.Transactions{}
				r, _ := regexp.MatchString(`^[Qm\w+]{46}$`, data)
				if r {
					db.Where("txn = ?", v.Hash().String()).First(transaction)
					if transaction.Txn != "" {
						transaction.BlockNumber = blockInfo.Number().Int64()
						transaction.TransactionDate = int64(blockInfo.Time())
						db.Model(transaction).Where("txn = ?", transaction.Txn).Updates(transaction)
					} else {
						transaction.BlockNumber = blockInfo.Number().Int64()
						transaction.TransactionDate = int64(blockInfo.Time())
						transaction.Txn = v.Hash().String()
						transaction.To = v.To().String()
						if msg, err := v.AsMessage(types.HomesteadSigner{}); err == nil {
							transaction.From = msg.From().Hex()
						}
						transaction.Amount = v.Value().String()
						transaction.Data = data
						transaction.Type = model.TypeRegisterData
						transaction.IndexStatus = 0
						db.Create(transaction)
					}
					errTransaction := indexConf.transactionProcessing(transaction, int64(blockInfo.Time()), 10)
					if errTransaction != nil {
						fmt.Println(time.Now().String()+"| BlockNumber: "+
							blockInfo.Number().String(), transaction.Txn, errTransaction.Error())
					}
				} else if v.Value().Int64() > 0 {
					db.Where("txn = ?", v.Hash().String()).First(transaction)
					if transaction.Txn != "" {
						transaction.BlockNumber = blockInfo.Number().Int64()
						transaction.TransactionDate = int64(blockInfo.Time())
						db.Model(transaction).Where("txn = ?", transaction.Txn).Updates(transaction)
					} else {
						transaction.BlockNumber = blockInfo.Number().Int64()
						transaction.To = v.To().String()
						transaction.Txn = v.Hash().String()
						transaction.TransactionDate = int64(blockInfo.Time())
						if msg, err := v.AsMessage(types.HomesteadSigner{}); err == nil {
							transaction.From = msg.From().Hex()
						}
						transaction.Amount = v.Value().String()
						transaction.Type = model.TypeMoneyTransfer
						transaction.IndexStatus = 1
						db.Create(transaction)
					}
				}
			}
		}
		db.Model(settings).Update("value", strconv.Itoa(i))
		indexNumber = i
	}
	RespondData(w, http.StatusOK, indexNumber)
}

func Reserve(db *gorm.DB, client *ethclient.Client, ipfs *shell.Shell, w http.ResponseWriter) {
	transactions := []*model.Transactions{}
	db.Table("transactions").
		Where("type = ?", model.TypeRegisterData).
		Where("index_status = ?", 0).
		Where("block_number != ?", 0).
		Find(&transactions)
	indexConf := indexConf{DB: db, IPFS: ipfs}
	for _, t := range transactions {
		block, err := client.BlockByNumber(context.Background(), big.NewInt(t.BlockNumber))
		if err != nil {
			HttpError(w, http.StatusBadRequest, err.Error())
			return
		}
		errTransaction := indexConf.transactionProcessing(t, int64(block.Time()), 30)
		if errTransaction != nil {
			fmt.Println(time.Now().String()+"| Reserve:BlockNumber: "+block.Number().String(), t.Txn, errTransaction.Error())
		}
	}
	RespondString(w, "end")
}

func (conf *indexConf) transactionProcessing(t *model.Transactions, tDate int64, sec int) error {
	conf.IPFS.SetTimeout(time.Duration(sec) * time.Second)
	cat, catErr := conf.IPFS.Cat(t.Data)
	if catErr != nil {
		return catErr
	} else {
		_ = conf.IPFS.Pin(t.Data)
		body, err := ioutil.ReadAll(cat)
		cat.Close()
		if err != nil {
			return err
		}
		animalIPFS := &model.AnimalIPFS{}
		animalsField := []model.AnimalsField{}
		errJson := json.Unmarshal(body, &animalIPFS)
		if errJson != nil {
			return errJson
		}
		transponder := &model.Transponders{}
		transponder.Transponder = animalIPFS.Transponder
		transponder.RegisterDate = tDate
		conf.DB.Where("transponder = ?", animalIPFS.Transponder).FirstOrCreate(transponder)
		relation := &model.TranspondersWallets{
			Transponder: animalIPFS.Transponder,
			Address:     t.From,
		}
		conf.DB.Where("transponder = ?", relation.Transponder).Where("address = ?", relation.Address).FirstOrCreate(relation)

		conf.DB.Find(&animalsField)
		for _, field := range animalsField {
			value := setValueAnimalField(field.Title, animalIPFS)
			if field.Type == "string" && len(value) > 0 || field.Type == "int" && value != "0" {
				animal := &model.Animals{}
				animal.TransponderID = animalIPFS.Transponder
				animal.TxnID = t.Txn
				animal.FileHash = t.Data
				animal.FieldID = field.Title
				animal.Value = value
				conf.DB.Create(animal)
			}
		}
		conf.DB.Model(&t).Where("txn = ?", t.Txn).UpdateColumn("index_status", 1)
	}
	return nil
}

func setValueAnimalField(title string, animalIPFS *model.AnimalIPFS) (res string) {
	switch title {
	case "itis_tsn":
		res = strconv.Itoa(animalIPFS.ItisTSN)
		break
	case "breed":
		res = animalIPFS.Breed
		break
	case "gender":
		res = animalIPFS.Gender
		break
	case "tag":
		res = animalIPFS.Tag
		break
	case "nickname":
		res = animalIPFS.Nickname
		break
	case "birth_date":
		res = strconv.Itoa(int(animalIPFS.BirthDate))
		break
	case "death_date":
		res = strconv.Itoa(int(animalIPFS.DeathDate))
		break
	case "sterilization":
		res = strconv.Itoa(int(animalIPFS.Sterilization))
		break
	case "vaccination":
		res = animalIPFS.Vaccination
		break
	case "father_transponder":
		res = animalIPFS.FatherTransponder
		break
	case "mother_transponder":
		res = animalIPFS.MotherTransponder
		break
	case "color":
		res = animalIPFS.Color
		break
	case "blood":
		res = animalIPFS.Blood
		break
	case "advanced_info":
		res = animalIPFS.AdvancedInfo
		break
	}
	return res
}
