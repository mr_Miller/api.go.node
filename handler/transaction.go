package handler

import (
	"api.go.node/app/model"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/ipfs/go-ipfs-api"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"math/big"
	"net/http"
	"strings"
	"sync"
	"time"
)

const (
	CONTRACT_ADDR = `0x2261633b519e8b95ff868c6ac6fd116a96e3e4b7` // addr smart contract
)

type Transaction interface {
	Send()
	SendAnimals()
}

type TransactionData struct {
	DB       *gorm.DB
	Identity *model.Users
	Client   *ethclient.Client
	//RPC          *rpc.Client
	Shell        *shell.Shell
	PathKeyStore string
	mux          sync.Mutex
}

type TransactionCoin struct {
	DB           *gorm.DB
	Identity     *model.Users
	Client       *ethclient.Client
	RPC          *rpc.Client
	PathKeyStore string
}

type params struct {
	Animal   *model.AnimalIPFS `json:"animal"`
	Callback string            `json:"callback"`
	Sandbox  int               `json:"sandbox"`
}

type params_batch struct {
	Animals  []*model.AnimalIPFS `json:"animals"`
	Callback string              `json:"callback"`
	Sandbox  int                 `json:"sandbox"`
}

type TransactionResult struct {
	TransactionHash string `json:"transaction_hash,omitempty"`
	FileHash        string `json:"file_hash,omitempty"`
	Transponder     string `json:"transponder,omitempty"`
	Error           string `json:"error,omitempty"`
}

func CreateCallbackLogs(db *gorm.DB, code, userId int, data, response string) error {
	err := db.Create(&model.CallbackLogs{
		Code:     code,
		UserID:   userId,
		Data:     data,
		Response: response,
	}).Error
	if err != nil {
		fmt.Println(time.Now().String(), "|ErrorCreateCallbackLogs: ", err.Error())
	}
	return err
}

func (t *TransactionData) Send(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	wallet := &model.Wallets{}
	t.DB.Where("user_id = ?", t.Identity.ID).First(wallet)
	if !wallet.Unlock {
		HttpError(w, http.StatusBadRequest, "Your wallet is locked")
		return
	}
	p := params{}
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &p)
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	if p.Callback == "" {
		HttpError(w, http.StatusBadRequest, "Callback URL is empty")
		return
	}
	go t.generateRequest(ctx, wallet, p)
	RespondString(w, "The animal performs the registration")
}

func (t *TransactionData) generateRequest(ctx context.Context, wallet *model.Wallets, p params) {
	hashFile, txn, err := t.newAnimal(ctx, wallet, p.Animal, p.Sandbox)
	result := TransactionResult{Transponder: p.Animal.Transponder}
	if err != nil {
		result.Error = err.Error()
	} else {
		result.FileHash = hashFile
		result.TransactionHash = txn
	}
	payloadBytes, _ := json.Marshal(result)
	req, err := http.NewRequest("POST", p.Callback, bytes.NewReader(payloadBytes))
	if err != nil {
		fmt.Println(time.Now().String(), "|ErrorNewRequest: ", err.Error())
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+t.Identity.AuthKey)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(time.Now().String(), "|ErrorDefaultClient: ", err.Error())
		_ = CreateCallbackLogs(t.DB, resp.StatusCode, t.Identity.ID, string(payloadBytes), err.Error())
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		respJson, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(time.Now().String(), "|ErrorReadAll: ", err.Error())
		}
		_ = CreateCallbackLogs(t.DB, resp.StatusCode, t.Identity.ID, string(payloadBytes), string(respJson))
	}
}

func (t *TransactionData) newAnimal(
	ctx context.Context, wallet *model.Wallets, animal *model.AnimalIPFS, sandbox int) (string, string, error) {
	isValid, err := model.ValidateAnimalIPFS(animal)
	if isValid {
		jsonAnimal, _ := json.Marshal(animal)
		dataAnimal := string(jsonAnimal)
		hashFile, err := t.Shell.Add(strings.NewReader(dataAnimal))
		if err != nil {
			return "", "", err
		}
		toAccDef := accounts.Account{
			Address: common.HexToAddress(CONTRACT_ADDR),
		}
		keyJson, readErr := ioutil.ReadFile(t.PathKeyStore + wallet.Filename)
		if readErr != nil {
			return "", "", readErr
		}
		keyWrapper, keyErr := keystore.DecryptKey(keyJson, wallet.Password)
		if keyErr != nil {
			return "", "", keyErr
		}
		if sandbox == 0 {
			signedTx, txErr := t.generateTransaction(ctx, keyWrapper, toAccDef, hashFile)
			if txErr != nil && txErr.Error() == "replacement transaction underpriced" {
				for i := 0; i < 5; i++ {
					signedTx, txErr = t.generateTransaction(ctx, keyWrapper, toAccDef, hashFile)
					if txErr == nil {
						break
					}
				}
				if txErr != nil {
					return "", "", txErr
				}
			} else if txErr != nil {
				return "", "", txErr
			}
			err = t.DB.Create(&model.Transactions{
				Txn:    signedTx.Hash().Hex(),
				From:   wallet.Address,
				To:     CONTRACT_ADDR,
				Amount: signedTx.Value().String(),
				Data:   hashFile,
				Type:   model.TypeRegisterData,
			}).Error
			if err != nil {
				return "", "", err
			}
			return hashFile, signedTx.Hash().String(), nil
		} else {
			return "SandboxFileHash", "SandboxTransactionHash", nil
		}
	}
	return "", "", err
}

func (t TransactionData) generateTransaction(
	ctx context.Context, keyWrapper *keystore.Key, toAccDef accounts.Account, hashFile string) (*types.Transaction, error) {
	nonce, errTc := t.Client.PendingNonceAt(context.Background(), keyWrapper.Address)
	if errTc != nil {
		return nil, errTc
	}
	value := big.NewInt(0)
	gasLimit := uint64(2000000)
	gasPrice, err := t.Client.SuggestGasPrice(ctx)
	if err != nil {
		return nil, err
	}
	tx := types.NewTransaction(uint64(nonce), toAccDef.Address, value, gasLimit, gasPrice, []byte(hashFile))
	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, keyWrapper.PrivateKey)
	if err != nil {
		return nil, err
	}
	txErr := t.Client.SendTransaction(ctx, signedTx)
	if txErr != nil {
		time.Sleep(time.Duration(5) * time.Second)
		return nil, txErr
	}
	return signedTx, nil
}

func (t *TransactionCoin) Send(w http.ResponseWriter, r *http.Request) {
	wallet := &model.Wallets{UserID: t.Identity.ID}
	t.DB.Where("user_id = ?", t.Identity.ID).First(wallet)
	if !wallet.Unlock {
		HttpError(w, http.StatusBadRequest, "Your wallet locked")
		return
	}
	var form struct {
		ToAddress string `json:"to_address"`
		Value     string `json:"value"`
	}
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&form); err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	value := StringToBigInt(form.Value)
	toAccDef := accounts.Account{
		Address: common.HexToAddress(form.ToAddress),
	}
	keyJson, readErr := ioutil.ReadFile(t.PathKeyStore + wallet.Filename)
	if readErr != nil {
		HttpError(w, http.StatusBadRequest, readErr.Error())
		return
	}
	keyWrapper, keyErr := keystore.DecryptKey(keyJson, wallet.Password)
	if keyErr != nil {
		HttpError(w, http.StatusBadRequest, keyErr.Error())
		return
	}
	ctx := context.Background()
	nonce, errTc := t.Client.PendingNonceAt(context.Background(), keyWrapper.Address)
	if errTc != nil {
		HttpError(w, http.StatusBadRequest, errTc.Error())
		return
	}
	gasLimit := uint64(2000000)
	gasPrice, err := t.Client.SuggestGasPrice(ctx)
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	var data []byte
	tx := types.NewTransaction(uint64(nonce), toAccDef.Address, value, gasLimit, gasPrice, data)
	signedTx, err := types.SignTx(tx, types.HomesteadSigner{}, keyWrapper.PrivateKey)
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	txErr := t.Client.SendTransaction(ctx, signedTx)
	if txErr != nil {
		HttpError(w, http.StatusBadRequest, txErr.Error())
		return
	}
	transaction := &model.Transactions{
		Txn:    signedTx.Hash().Hex(),
		From:   wallet.Address,
		To:     form.ToAddress,
		Amount: value.String(),
		Data:   "",
		Type:   model.TypeMoneyTransfer,
	}
	t.DB.Create(transaction)
	RespondString(w, signedTx.Hash().Hex())
}

//func GetTransactionsByAddress(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
//	vars := mux.Vars(r)
//	transactions := []*model.Transactions{}
//	address := vars["address"]
//	db.Where(&model.Transactions{From: address}).Or(&model.Transactions{To: address}).Find(&transactions)
//	RespondData(w, http.StatusOK, transactions)
//}

/***************************************************************************************************************/
func (t *TransactionData) SendAnimals(w http.ResponseWriter, r *http.Request) {
	wallet := &model.Wallets{}
	t.DB.Where("user_id = ?", t.Identity.ID).First(wallet)
	if !wallet.Unlock {
		HttpError(w, http.StatusBadRequest, "Your wallet is locked")
		return
	}
	p := params_batch{}
	body, _ := ioutil.ReadAll(r.Body)
	err := json.Unmarshal(body, &p)
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	if len(p.Animals) > 1000 {
		HttpError(w, http.StatusUnprocessableEntity, "The maximum number of animals to be no more than 1000 at a time")
		return
	}

	if p.Callback == "" {
		HttpError(w, http.StatusBadRequest, "Callback URL is empty")
		return
	}
	go t.generateAnimalRequest(wallet, p)
	RespondString(w, "Animals are being registered")
}

func (t *TransactionData) generateAnimalRequest(wallet *model.Wallets, p params_batch) {
	ctx := context.Background()
	result := []*TransactionResult{}
	header, err := t.Client.HeaderByNumber(ctx, nil)
	if err != nil {
		fmt.Println(time.Now().String(), "|HeaderByNumber: ", err.Error())
	}
	for i, animal := range p.Animals {
		hashFile, txn, err := t.createAnimal(ctx, wallet, animal, header, uint64(i), p.Sandbox)
		//fmt.Println("Animal: ", i, " Transponder: ", animal.Transponder)
		if err != nil {
			//fmt.Println("Error: ", err.Error())
			result = append(result, &TransactionResult{
				Transponder: animal.Transponder,
				Error:       err.Error(),
			})
		} else {
			//fmt.Println("File: ", hashFile, " Transaction: ", txn)
			result = append(result, &TransactionResult{
				Transponder:     animal.Transponder,
				TransactionHash: txn,
				FileHash:        hashFile,
			})
		}
	}
	payloadBytes, _ := json.Marshal(result)
	req, err := http.NewRequest("POST", p.Callback, bytes.NewReader(payloadBytes))
	if err != nil {
		fmt.Println(time.Now().String(), "|ErrorNewRequest: ", err.Error())
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+t.Identity.AuthKey)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(time.Now().String(), "|ErrorDefaultClient: ", err.Error())
		_ = CreateCallbackLogs(t.DB, resp.StatusCode, t.Identity.ID, string(payloadBytes), err.Error())
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		respJson, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(time.Now().String(), "|ErrorReadAll: ", err.Error())
		}
		_ = CreateCallbackLogs(t.DB, resp.StatusCode, t.Identity.ID, string(payloadBytes), string(respJson))
	}
}

func (t *TransactionData) createAnimal(
	ctx context.Context, wallet *model.Wallets, animal *model.AnimalIPFS, header *types.Header,
	index uint64, sandbox int) (string, string, error) {

	t.mux.Lock()
	defer t.mux.Unlock()
	isValid, err := model.ValidateAnimalIPFS(animal)
	if isValid {
		jsonAnimal, _ := json.Marshal(animal)
		dataAnimal := string(jsonAnimal)
		hashFile, err := t.Shell.Add(strings.NewReader(dataAnimal))
		if err != nil {
			return "", "", err
		}
		toAccDef := accounts.Account{
			Address: common.HexToAddress(CONTRACT_ADDR),
		}
		keyJson, readErr := ioutil.ReadFile(t.PathKeyStore + wallet.Filename)
		if readErr != nil {
			return "", "", readErr
		}
		keyWrapper, keyErr := keystore.DecryptKey(keyJson, wallet.Password)
		if keyErr != nil {
			return "", "", keyErr
		}

		nonce, _ := t.Client.NonceAt(context.Background(), keyWrapper.Address, header.Number)
		if sandbox == 0 {
			signedTx, txErr := t.createTransaction(ctx, keyWrapper, toAccDef, hashFile, nonce+index)
			if txErr != nil {
				return "", "", txErr
			}
			err = t.DB.Create(&model.Transactions{
				Txn:    signedTx.Hash().Hex(),
				From:   wallet.Address,
				To:     CONTRACT_ADDR,
				Amount: signedTx.Value().String(),
				Data:   hashFile,
				Type:   model.TypeRegisterData,
			}).Error
			if err != nil {
				return "", "", err
			}
			return hashFile, signedTx.Hash().String(), nil
		} else {
			return "SandboxFileHash", "SandboxTransactionHash", nil
		}
	}
	return "", "", err
}

func (t TransactionData) createTransaction(
	ctx context.Context, keyWrapper *keystore.Key, toAccDef accounts.Account, hashFile string,
	nonce uint64) (*types.Transaction, error) {

	value := big.NewInt(0)
	gasLimit := uint64(40000)
	gasPrice, err := t.Client.SuggestGasPrice(ctx)
	if err != nil {
		return nil, err
	}
	tx := types.NewTransaction(nonce, toAccDef.Address, value, gasLimit, gasPrice, []byte(hashFile))
	signedTx, err := types.SignTx(tx, types.NewEIP155Signer(big.NewInt(901025)), keyWrapper.PrivateKey)
	if err != nil {
		return nil, err
	}
	txErr := t.Client.SendTransaction(ctx, signedTx)
	if txErr != nil {
		return nil, txErr
	}
	return signedTx, nil
}
