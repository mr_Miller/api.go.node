package handler

import (
	"api.go.node/app/model"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
)

func getDocumentOr404(db *gorm.DB, id string, w http.ResponseWriter, r *http.Request) *model.Documents {
	doc := &model.Documents{}
	if err := db.First(doc, "id = ?", id).Error; err != nil {
		HttpError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return doc
}

func GetDocument(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	filePath, _ := filepath.Abs("./uploads/documents")
	document := getDocumentOr404(db, vars["id"], w, r)
	docPath := filePath + "/" + strconv.Itoa(document.UserID) + "/" + document.Name
	f, err := os.Open(docPath)
	if err != nil {
		HttpError(w, http.StatusNotFound, err.Error())
	}
	defer f.Close()
	fi, err := f.Stat()
	if err != nil {
		HttpError(w, http.StatusNotFound, err.Error())
	}
	w.Header().Set("Content-Disposition", "attachment; filename=test.jpg")
	http.ServeContent(w, r, document.Name, fi.ModTime(), f)
}

func AddDocuments(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	filePath, _ := filepath.Abs("./uploads/documents")
	path := filePath + "/" + vars["id"] + "/"
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, 0755)
	}
	err := r.ParseMultipartForm(5 << 10) // grab the multipart form
	if err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
		return
	}
	formData := r.MultipartForm
	for _, files := range formData.File {
		if files[0].Size > 5*1024*1024 {
			HttpError(w, http.StatusBadRequest, "File size must not exceed 5 MB")
			return
		}

		file, err := files[0].Open()
		if err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}
		defer file.Close()
		fileHeader := make([]byte, 512)
		if _, err := file.Read(fileHeader); err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}

		if http.DetectContentType(fileHeader) != "application/pdf" {
			HttpError(w, http.StatusInternalServerError, "Must be only PDF files")
			return
		}
	}

	if len(formData.File) < 1 {
		HttpError(w, http.StatusBadRequest, "Minimum must be 1 file")
		return
	}

	if len(formData.File) > 10 {
		HttpError(w, http.StatusBadRequest, "There should be no more than 10 files")
		return
	}

	docs := []*model.Documents{}
	for _, files := range formData.File {
		file, err := files[0].Open()
		defer file.Close()
		if err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}
		out, err := os.Create(path + files[0].Filename)
		defer out.Close()
		if err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}
		_, err = io.Copy(out, file)
		if err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}
		id, _ := strconv.Atoi(vars["id"])
		document := &model.Documents{Name: files[0].Filename, UserID: id}
		db.FirstOrCreate(document, "name = ?", files[0].Filename)
		docs = append(docs, document)

	}
	RespondData(w, http.StatusOK, docs)
}

func RemoveDocument(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	document := getDocumentOr404(db, vars["id"], w, r)
	if document == nil {
		return
	}
	if err := db.Delete(&document).Error; err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
		return
	}
	RespondBool(w, true)
}
