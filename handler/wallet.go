package handler

import (
	"api.go.node/app/model"
	"context"
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/jinzhu/gorm"
	"math"
	"net/http"
	"strconv"
	"strings"
)

type BalanceUser struct {
	Address string `json:"address"`
	Balance string `json:"balance"`
	Unlock  bool   `json:"unlock"`
}

type Wallet struct {
	Account      BalanceUser           `json:"account"`
	Transactions []*model.Transactions `json:"transactions"`
	PageCount    int                   `json:"page_count"`
}

func CreateWallet(user *model.Users, db *gorm.DB, keyStore *keystore.KeyStore) (wallet *model.Wallets, err error) {
	password := user.Password
	newAccount, err := keyStore.NewAccount(password)
	if err != nil {
		return nil, err
	}
	address := accounts.Account{
		Address: common.HexToAddress(newAccount.Address.String()),
	}
	signAcc, err := keyStore.Find(address)
	if err != nil {
		return nil, err
	}
	errUnlock := keyStore.Unlock(signAcc, password)
	if errUnlock != nil {
		return nil, err
	}
	filenameArray := strings.Split(newAccount.URL.Path, "/")
	wallet = &model.Wallets{
		UserID:   user.ID,
		Address:  newAccount.Address.String(),
		Password: password,
		Filename: filenameArray[len(filenameArray)-1],
		Unlock:   true,
	}
	if err := db.Create(wallet).Error; err != nil {
		return nil, err
	}
	return user.Wallet, nil
}

func FindAccount(db *gorm.DB, client *ethclient.Client, Identity *model.Users) *BalanceUser {
	wallet := &model.Wallets{}
	db.Select("address, unlock").Where("user_id = ?", Identity.ID).First(wallet)
	address := common.HexToAddress(wallet.Address)
	balance, _ := client.BalanceAt(context.Background(), address, nil)
	return &BalanceUser{Address: wallet.Address, Balance: balance.String(), Unlock: wallet.Unlock}
}

func BalanceAccount(client *ethclient.Client, address common.Address, w http.ResponseWriter) {
	balance, err := client.BalanceAt(context.Background(), address, nil)
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	RespondData(w, http.StatusOK, &BalanceUser{
		Address: address.String(),
		Balance: balance.String(),
	})
}

func (account BalanceUser) MyWallet(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	perPage := 8
	var txnCount int
	transactions := []*model.Transactions{}
	db.Where("LOWER(\"from\") = LOWER(?) AND type = ?", account.Address, model.TypeMoneyTransfer).
		Or("LOWER(\"to\") = LOWER(?) AND type = ?", account.Address, model.TypeMoneyTransfer).
		Order("transaction_date DESC").
		Limit(perPage).
		Offset(page * perPage).
		Find(&transactions).
		Limit(-1).Offset(-1).Count(&txnCount)
	pages := math.Round(float64(txnCount / perPage))
	perPageCount := float64(float64(txnCount) / pages)
	if perPageCount > float64(perPage) {
		pages += 1
	}
	RespondData(w, http.StatusOK, Wallet{
		Account:      account,
		Transactions: transactions,
		PageCount:    int(pages),
	})
}
