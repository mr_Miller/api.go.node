package handler

import (
	"api.go.node/app/model"
	"github.com/jinzhu/gorm"
	"net/http"
)

func Countries(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	type Result struct {
		Value int    `json:"value"`
		Label string `json:"label"`
	}
	res := []Result{}
	db.Model(model.CountryNames{}).
		Select("country_id AS Value, name AS Label").
		Where("lang = ?", r.URL.Query().Get("lang")).
		Scan(&res)
	RespondData(w, http.StatusOK, &res)
}
