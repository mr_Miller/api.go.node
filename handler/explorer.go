package handler

import (
	"context"
	"encoding/hex"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/gorilla/mux"
	"math"
	"math/big"
	"net/http"
	"strconv"
	"strings"
)

type eTransactions struct {
	TxHash      string   `json:"tx_hash,omitempty"`
	From        string   `json:"from,omitempty"`
	To          string   `json:"to,omitempty"`
	Value       string   `json:"value,omitempty"`
	BlockNumber *big.Int `json:"block_number,omitempty"`
	Time        uint64   `json:"time,omitempty"`
	TxStatus    bool     `json:"tx_status,omitempty"`
	GasPrice    *big.Int `json:"gas_price,omitempty"`
	TxCost      *big.Int `json:"cost,omitempty"`
	Nonce       uint64   `json:"nonce,omitempty"`
	InputData   string   `json:"data,omitempty"`
}

type eBlock struct {
	Number       *big.Int         `json:"number"`
	Time         uint64           `json:"time"`
	Hash         string           `json:"hash"`
	ParentHash   string           `json:"parent_hash"`
	UnclesHash   string           `json:"uncles_hash"`
	Miner        string           `json:"miner"`
	Difficulty   *big.Int         `json:"difficulty"`
	Size         string           `json:"size"`
	GasUsed      uint64           `json:"gas_used"`
	GasLimit     uint64           `json:"gas_limit"`
	Nonce        uint64           `json:"nonce"`
	ExtraData    string           `json:"extra_data"`
	Transactions []*eTransactions `json:"transactions"`
}

func ExplorerBlocks(client *ethclient.Client, w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	type InfoBlocks struct {
		BlockNumber *big.Int `json:"block_number"`
		BlockHash   string   `json:"block_hash"`
		TxnCount    int      `json:"txn"`
		Time        uint64   `json:"time"`
	}
	type BlocksResult struct {
		LastBlock int64         `json:"last_block"`
		Blocks    []*InfoBlocks `json:"blocks"`
		PageCount float64       `json:"page_count"`
	}
	result := &BlocksResult{}
	perPage := 16

	ctx := context.Background()
	header, err := client.HeaderByNumber(ctx, nil)
	if err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
	}
	result.LastBlock = header.Number.Int64()
	result.PageCount = math.Round(float64(int(result.LastBlock) / perPage))

	perPageCount := float64(int(result.LastBlock)) / result.PageCount
	if perPageCount > float64(perPage) {
		result.PageCount += 1
	}

	i := int(result.LastBlock) - (perPage * page)
	k := i - perPage
	if i < perPage {
		k = 0
	}
	for ; i > k; i-- {
		block, _ := client.BlockByNumber(ctx, big.NewInt(int64(i)))
		infoBlocks := &InfoBlocks{
			BlockNumber: block.Number(),
			BlockHash:   block.Hash().String(),
			TxnCount:    len(block.Transactions()),
			Time:        block.Time(),
		}
		result.Blocks = append(result.Blocks, infoBlocks)
	}

	RespondData(w, http.StatusOK, result)
}

func BlockByNumber(client *ethclient.Client, number int, w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	block, err := client.BlockByNumber(ctx, big.NewInt(int64(number)))
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
	}
	chainID, _ := client.NetworkID(ctx)
	result := getBlock(block, chainID)
	RespondData(w, http.StatusOK, result)
}

func BlockByHash(client *ethclient.Client, hash string, w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	block, err := client.BlockByHash(ctx, common.HexToHash(hash))
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
	}
	chainID, _ := client.NetworkID(ctx)
	result := getBlock(block, chainID)
	RespondData(w, http.StatusOK, result)
}

func getBlock(block *types.Block, chainID *big.Int) *eBlock {
	result := &eBlock{
		Number:     block.Number(),
		Time:       block.Time(),
		Hash:       block.Hash().String(),
		ParentHash: block.ParentHash().String(),
		UnclesHash: block.UncleHash().String(),
		Miner:      block.Coinbase().String(),
		Difficulty: block.Difficulty(),
		Size:       block.Size().String(),
		GasUsed:    block.GasUsed(),
		GasLimit:   block.GasLimit(),
		Nonce:      block.Nonce(),
		ExtraData:  "0x" + hex.EncodeToString(block.Extra()),
	}

	for _, tx := range block.Transactions() {
		if msg, err := tx.AsMessage(types.NewEIP155Signer(chainID)); err == nil {
			result.Transactions = append(result.Transactions, &eTransactions{
				TxHash: tx.Hash().String(),
				From:   msg.From().Hex(),
				To:     tx.To().String(),
				Value:  tx.Value().String(),
			})
		}
	}
	return result
}

func TransactionInfo(client *ethclient.Client, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	blockNumber, _ := strconv.Atoi(vars["block"])
	hash := vars["tx"]
	ctx := context.Background()
	block, _ := client.BlockByNumber(ctx, big.NewInt(int64(blockNumber)))
	result, _ := getTransaction(ctx, client, block, common.HexToHash(hash))
	RespondData(w, http.StatusOK, result)
}

func ExplorerSearch(client *ethclient.Client, rpc *rpc.Client, w http.ResponseWriter, r *http.Request) {
	query := strings.TrimSpace(r.URL.Query().Get("query"))
	blockNumber, isString := strconv.Atoi(query)
	if isString != nil {
		ctx := context.Background()
		block, isTx := client.BlockByHash(ctx, common.HexToHash(query))
		if isTx != nil {
			type txExtraInfo struct {
				BlockNumber *string     `json:"blockNumber,omitempty"`
				BlockHash   common.Hash `json:"blockHash,omitempty"`
			}
			info := txExtraInfo{}
			rpc.CallContext(ctx, &info, "eth_getTransactionByHash", common.HexToHash(query))
			block, err := client.BlockByHash(ctx, info.BlockHash)
			if err != nil {
				HttpError(w, http.StatusBadRequest, err.Error())
				return
			}
			result, err := getTransaction(ctx, client, block, common.HexToHash(query))
			if err != nil {
				HttpError(w, http.StatusBadRequest, err.Error())
				return
			}
			RespondData(w, http.StatusOK, result)
			return
		} else {
			chainID, _ := client.NetworkID(ctx)
			result := getBlock(block, chainID)
			RespondData(w, http.StatusOK, result)
			return
		}
	} else {
		BlockByNumber(client, blockNumber, w, r)
		return
	}
	HttpError(w, http.StatusBadRequest, "NotFound")
}

func getTransaction(ctx context.Context,
	client *ethclient.Client, block *types.Block, txHash common.Hash) (*eTransactions, error) {
	tx, pending, err := client.TransactionByHash(ctx, txHash)
	if err != nil {
		return nil, err
	}
	chainID, _ := client.NetworkID(ctx)
	msg, err := tx.AsMessage(types.NewEIP155Signer(chainID))
	if err != nil {
		return nil, err
	}
	result := &eTransactions{
		TxHash:      tx.Hash().String(),
		From:        msg.From().String(),
		To:          tx.To().String(),
		Value:       tx.Value().String(),
		BlockNumber: block.Number(),
		Time:        block.Time(),
		TxStatus:    pending,
		GasPrice:    tx.GasPrice(),
		TxCost:      tx.Cost(),
		Nonce:       tx.Nonce(),
		InputData:   "0x" + hex.EncodeToString(tx.Data()),
	}
	return result, nil
}
