package handler

import (
	"api.go.node/app/model"
	"context"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/jinzhu/gorm"
	"net/http"
)

type statisticResult struct {
	Markers []model.Users `json:"markers"`
	Animals int           `json:"animals"`
	Members int           `json:"members"`
	Blocks  int64         `json:"blocks"`
	Nodes   int           `json:"nodes"`
}

func GetStatisticsMap(db *gorm.DB, client *ethclient.Client, w http.ResponseWriter, r *http.Request) {
	//Markers
	users := []model.Users{}
	db.Preload("UserInfo", func(db *gorm.DB) *gorm.DB {
		return db.Preload("CountryNames", func(db *gorm.DB) *gorm.DB {
			return db.Select("country_id, name, lang")
		}).Select("id, user_id, ownership, website, name, country_iso, city, street, house_number, start_time, end_time, lat, lng")
	}).Select("id, email, phone, role, verified_by_admin, created_at").Order("created_at desc").
		Where("verified_by_admin = ?", true).Where("role !=?", "admin").Find(&users)

	//Blocks
	header, err := client.HeaderByNumber(context.Background(), nil)
	if err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
	}
	lastBlock := header.Number.Int64()

	//registered animals
	var animals int
	db.Table("transponders").Count(&animals)

	// nodes
	var nodes int = 5
	for _, u := range users {
		if u.Role == "node" {
			nodes += 1
		}
	}

	result := statisticResult{
		Markers: users,
		Animals: animals,
		Members: len(users),
		Blocks:  lastBlock,
		Nodes:   nodes,
	}
	RespondData(w, http.StatusOK, result)
}
