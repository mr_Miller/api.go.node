package handler

import (
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"net/http"
)

func NewAccount(db *gorm.DB, keyStore *keystore.KeyStore, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user := getUserOr404(db, vars["id"], w, r)
	if user.Wallet != nil {
		HttpError(w, http.StatusBadRequest, "Wallet already exists")
		return
	}
	wallet, err := CreateWallet(user, db, keyStore)
	if err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
	}
	RespondData(w, http.StatusOK, wallet)
}

func LockedBcAccount(ks *keystore.KeyStore, w http.ResponseWriter, r *http.Request) {
	address := r.FormValue("address")
	if ks.HasAddress(common.HexToAddress(address)) {
		err := ks.Lock(common.HexToAddress(address))
		if err != nil {
			HttpError(w, http.StatusBadRequest, err.Error())
			return
		}
		RespondString(w, "USER_LOCKED")
	} else {
		HttpError(w, http.StatusUnprocessableEntity, "ADDRESS_NOT_EXIST")
	}
}

func UnlockedBcAccount(ks *keystore.KeyStore, w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	address := r.FormValue("address")
	password := r.FormValue("password")
	account := accounts.Account{
		Address: common.HexToAddress(address),
	}
	signAcc, errF := ks.Find(account)
	if errF != nil {
		HttpError(w, http.StatusBadRequest, errF.Error())
		return
	}
	errU := ks.Unlock(signAcc, password)
	if errU != nil {
		HttpError(w, http.StatusBadRequest, errU.Error())
		return
	}
	RespondString(w, "USER_UNLOCKED")
}
