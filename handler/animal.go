package handler

import (
	"api.go.node/app/model"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"math"
	"net/http"
	"strconv"
)

func GetAnimalProfile(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	type animalResult struct {
		Animal       *model.AnimalIPFS    `json:"animal"`
		Transactions []model.Transactions `json:"transactions"`
	}
	vars := mux.Vars(r)
	animals := []model.Animals{}
	transactions := []model.Transactions{}
	db.Preload("Animals", func(db *gorm.DB) *gorm.DB {
		return db.Select("txn_id, field_id, value")
	}).Select("txn, block_number, \"from\", transaction_date").
		Where("txn IN (SELECT a.txn_id FROM animals AS a WHERE a.transponder_id = ?)", vars["id"]).
		Where("index_status = ? AND type = ?", 1, model.TypeRegisterData).
		Order("created_at ASC").
		Find(&transactions)

	db.Select("transponder_id, field_id, value").
		Where("transponder_id = ?", vars["id"]).
		Where("id IN (SELECT max(a2.id) FROM animals AS a2 WHERE a2.transponder_id = ? GROUP BY a2.field_id)", vars["id"]).
		Find(&animals)

	var registerDate int64
	db.Table("transponders").Select("register_date").Where("transponder = ?", vars["id"]).Row().Scan(&registerDate)

	if len(animals) <= 0 {
		HttpError(w, http.StatusBadRequest, "NotFound")
		return
	}

	animal := &model.AnimalIPFS{}
	animal.Transponder = animals[1].TransponderID
	animal.RegisterDate = registerDate
	for _, a := range animals {
		getFields(&a, animal)
	}
	RespondData(w, http.StatusOK, animalResult{Animal: animal, Transactions: transactions})
}

func GetAnimalsByUser(db *gorm.DB, Identity *model.Users, w http.ResponseWriter, r *http.Request) {
	type Result struct {
		Animals   []model.AnimalIPFS `json:"animals"`
		PageCount int                `json:"page_count"`
	}
	page, _ := strconv.Atoi(r.URL.Query().Get("page"))
	//sort, _ := strconv.Atoi(r.URL.Query().Get("sort"))
	limit := 16
	offset := limit * page
	animals := []model.AnimalIPFS{}
	allAnimals := 0
	db.Table("transponders_wallets AS tw").
		Select("tw.transponder, va.*, t.register_date").
		Joins("LEFT JOIN view_animals AS va ON va.transponder_id = tw.transponder").
		Joins("LEFT JOIN transponders AS t ON t.transponder = tw.transponder").
		Where("LOWER(tw.address) = LOWER(?)", Identity.Wallet.Address).
		Order("t.register_date DESC").
		Limit(limit).Offset(offset).Find(&animals).
		Limit(-1).Offset(-1).Count(&allAnimals)
	pages := math.Round(float64(allAnimals / limit))
	perPageCount := float64(float64(allAnimals) / pages)
	if perPageCount > float64(limit) {
		pages += 1
	}
	RespondData(w, http.StatusOK, &Result{
		Animals:   animals,
		PageCount: int(pages),
	})
}

func getFields(a *model.Animals, animal *model.AnimalIPFS) {
	switch a.FieldID {
	case "itis_tsn":
		value, _ := strconv.ParseInt(a.Value, 10, 64)
		animal.ItisTSN = int(value)
		break
	case "breed":
		animal.Breed = a.Value
		break
	case "gender":
		animal.Gender = a.Value
		break
	case "tag":
		animal.Tag = a.Value
		break
	case "nickname":
		animal.Nickname = a.Value
		break
	case "birth_date":
		value, _ := strconv.ParseInt(a.Value, 10, 64)
		animal.BirthDate = value
		break
	case "death_date":
		value, _ := strconv.ParseInt(a.Value, 10, 64)
		animal.DeathDate = value
		break
	case "sterilization":
		value, _ := strconv.ParseInt(a.Value, 10, 64)
		animal.Sterilization = value
		break
	case "vaccination":
		animal.Vaccination = a.Value
		break
	case "father_transponder":
		animal.FatherTransponder = a.Value
		break
	case "mother_transponder":
		animal.MotherTransponder = a.Value
		break
	case "blood":
		animal.Blood = a.Value
		break
	case "color":
		animal.Color = a.Value
		break
	case "advanced_info":
		animal.AdvancedInfo = a.Value
		break
	}
}
