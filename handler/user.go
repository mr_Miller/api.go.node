package handler

import (
	"api.go.node/app/model"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func getUserOr404(db *gorm.DB, id string, w http.ResponseWriter, r *http.Request) *model.Users {
	user := &model.Users{}
	if err := db.Preload("UserInfo").Preload("Wallet").First(user, id).Error; err != nil {
		HttpError(w, http.StatusNotFound, err.Error())
		return nil
	}
	return user
}

func generateAuthToken(db *gorm.DB) string {
	start := time.Now().String()
	hash := sha256.New()
	hash.Write([]byte(start))
	md := hash.Sum(nil)
	auth_key := hex.EncodeToString(md)
	if model.ValidateAuthToken(auth_key, db) {
		generateAuthToken(db)
	}
	return auth_key
}

func GetUsers(db *gorm.DB, w http.ResponseWriter) {
	users := []model.Users{}
	db.Preload("Wallet", func(db *gorm.DB) *gorm.DB { return db.Select("id, created_at, user_id, address") }).
		Select("id, email, phone, role, verified_by_admin, created_at").Order("created_at desc").Find(&users)
	RespondData(w, http.StatusOK, users)
}

func DetailUserById(db *gorm.DB, userId int, w http.ResponseWriter) {
	user := model.Users{}
	db.Preload("UserInfo").Preload("Documents").Preload("Wallet", func(db *gorm.DB) *gorm.DB {
		return db.Select("id, address")
	}).Select("id, phone, email, verified_by_admin, role").First(&user, userId)
	RespondData(w, http.StatusOK, &user)
}

func PublicProfileUserByAddress(db *gorm.DB, address string, w http.ResponseWriter) {
	wallet := model.Wallets{}
	db.
		Preload("Users", func(db *gorm.DB) *gorm.DB {
			return db.Preload("UserInfo", func(db *gorm.DB) *gorm.DB {
				return db.Preload("CountryNames", func(db *gorm.DB) *gorm.DB {
					return db.Select("country_id, name, lang")
				}).Select("id, user_id, ownership, website, name, country_iso, city, street, house_number, start_time, end_time, lat, lng")
			}).Select("id, phone, email, role, lang, created_at")
		}).
		Select("user_id, address").Where("address = ?", address).First(&wallet)
	RespondData(w, http.StatusOK, &wallet)
}

func VerifiedUserById(db *gorm.DB, keyStore *keystore.KeyStore, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user := getUserOr404(db, vars["id"], w, r)
	user.Verified()
	if err := db.Save(&user).Error; err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
		return
	}
	if user.Wallet == nil {
		_, err := CreateWallet(user, db, keyStore)
		if err != nil {
			HttpError(w, http.StatusBadRequest, err.Error())
		}
	}
	RespondBool(w, user.VerifiedByAdmin)
}

func LockUserById(db *gorm.DB, IdentityUser *model.Users, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, _ := strconv.Atoi(vars["id"])
	if id != IdentityUser.ID {
		user := getUserOr404(db, vars["id"], w, r)
		if user == nil {
			return
		}
		user.Restore()
		if err := db.Save(&user).Error; err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}
		RespondBool(w, user.VerifiedByAdmin)
	} else {
		HttpError(w, http.StatusForbidden, "You can not block yourself")
	}
}

func UpdateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	user := getUserOr404(db, vars["id"], w, r)
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&user); err != nil {
		HttpError(w, http.StatusBadRequest, err.Error())
		return
	}
	defer r.Body.Close()
	isValid, err := model.ValidateUser(user)
	if isValid {
		if err := db.Save(&user).Error; err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}
		RespondBool(w, true)
	} else {
		errs := strings.Split(err.Error(), ";")
		errors := make(map[string]string)
		for _, errorMsg := range errs {
			errMsg := strings.Split(errorMsg, ":")
			errors[errMsg[0]] = errMsg[1]
		}
		RespondData(w, http.StatusUnprocessableEntity, errors)
	}
}

func CreateUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	phone := r.FormValue("phone")
	email := r.FormValue("email")
	password, _ := HashPassword(r.FormValue("password"))
	role := r.FormValue("role")
	auth_token := generateAuthToken(db)

	ownership := r.FormValue("ownership")
	website := r.FormValue("website")
	name := r.FormValue("name")
	short_desc := r.FormValue("short_desc")
	description := r.FormValue("description")
	country_iso, _ := strconv.ParseInt(r.FormValue("country_iso"), 10, 32)
	city := r.FormValue("city")
	street := r.FormValue("street")
	house_number := r.FormValue("house_number")
	start_time := r.FormValue("start_time")
	end_time := r.FormValue("end_time")
	why_you := r.FormValue("why_you")

	err := r.ParseMultipartForm(5 << 10)
	if err != nil {
		HttpError(w, http.StatusInternalServerError, err.Error())
		return
	}
	formData := r.MultipartForm

	for _, files := range formData.File {
		if files[0].Size > 5*1024*1024 {
			HttpError(w, http.StatusBadRequest, "File size must not exceed 5 MB")
			return
		}

		file, err := files[0].Open()
		if err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}
		defer file.Close()
		fileHeader := make([]byte, 512)
		if _, err := file.Read(fileHeader); err != nil {
			HttpError(w, http.StatusInternalServerError, err.Error())
			return
		}

		if http.DetectContentType(fileHeader) != "application/pdf" {
			HttpError(w, http.StatusInternalServerError, "Must be only PDF files")
			return
		}
	}

	if len(formData.File) < 1 {
		HttpError(w, http.StatusBadRequest, "Minimum must be 1 file")
		return
	}

	if len(formData.File) > 10 {
		HttpError(w, http.StatusBadRequest, "There should be no more than 10 files")
		return
	}

	userInfo := &model.UserInfo{
		Ownership:   ownership,
		Website:     website,
		Name:        name,
		ShortDesc:   short_desc,
		Description: description,
		CountryISO:  uint(country_iso),
		City:        city,
		Street:      street,
		HouseNumber: house_number,
		StartTime:   start_time,
		EndTime:     end_time,
		WhyYou:      why_you,
		Users: &model.Users{
			Phone:              phone,
			Email:              email,
			Password:           password,
			AuthKey:            auth_token,
			PasswordResetToken: RandStringRunes(30),
			Role:               role,
		},
	}

	isValid, err := model.ValidateUserCreate(userInfo)
	if isValid {
		db.Create(userInfo)
		filePath, _ := filepath.Abs("./uploads/documents")
		path := filePath + "/" + strconv.Itoa(userInfo.Users.ID) + "/"
		if _, err := os.Stat(path); os.IsNotExist(err) {
			os.Mkdir(path, 0755)
		}
		for _, files := range formData.File {
			file, err := files[0].Open()
			defer file.Close()
			if err != nil {
				HttpError(w, http.StatusInternalServerError, err.Error())
				return
			}
			out, err := os.Create(path + files[0].Filename)
			defer out.Close()
			if err != nil {
				HttpError(w, http.StatusInternalServerError, err.Error())
				return
			}
			_, err = io.Copy(out, file)
			if err != nil {
				HttpError(w, http.StatusInternalServerError, err.Error())
				return
			}
			db.Create(&model.Documents{Name: files[0].Filename, UserID: userInfo.Users.ID})
		}
		RespondString(w, userInfo.Users.AuthKey)
	} else {
		errs := strings.Split(err.Error(), ";")
		errors := make(map[string]string)
		for _, errorMsg := range errs {
			errMsg := strings.Split(errorMsg, ":")
			errors[errMsg[0]] = errMsg[1]
		}
		RespondData(w, http.StatusUnprocessableEntity, errors)
	}
}
