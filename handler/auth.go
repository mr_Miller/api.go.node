package handler

import (
	"api.go.node/app/model"
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

func BasicAuth(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
	if len(auth) != 2 || auth[0] != "Basic" {
		HttpError(w, http.StatusUnauthorized, "Authorization failed")
		return
	}
	payload, _ := base64.StdEncoding.DecodeString(auth[1])
	pair := strings.SplitN(string(payload), ":", 2)
	if len(pair) != 2 {
		HttpError(w, http.StatusUnauthorized, "Authorization failed")
		return
	}
	var user = &model.Users{}
	db.Where("email = ?", pair[0]).First(user)
	if !CheckPasswordHash(pair[1], user.Password) {
		HttpError(w, http.StatusUnauthorized, "Email or Password incorrect")
		return
	}
	if !user.VerifiedByAdmin {
		HttpError(w, http.StatusUnauthorized, "Account is not verified by administration")
		return
	}
	type Result struct {
		UserId    int
		AuthToken string
		Role      string
		Lang      string
	}
	result := Result{UserId: user.ID, AuthToken: user.AuthKey, Role: user.Role, Lang: user.Lang}
	RespondData(w, http.StatusOK, result)
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-1234567890")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	s := md5.Sum([]byte(time.Now().String()))
	md5Hash := hex.EncodeToString(s[:])
	return md5Hash + string(b)
}
