package config

import (
	"github.com/tkanos/gonfig"
	"log"
	"path/filepath"
)

type Config struct {
	DB        *DBConfig
	EthClient *EthClientConf
	IPFS      *IPFSConfig
}

type DBConfig struct {
	Dialect  string
	Host     string
	Username string
	Password string
	Name     string
}

type EthClientConf struct {
	NodeAddress  string
	PathKeyStore string
}

type IPFSConfig struct {
	Url string
}

type Cfg struct {
	NodeAddress   string
	PathKeyStore  string
	IPFS_ShellUrl string
	DB_HOST       string
	DB_NAME       string
	DB_USER       string
	DB_PASSWORD   string
}

func GetConfig() *Config {
	var cfg = &Cfg{}
	filePath, _ := filepath.Abs("config.json")
	err := gonfig.GetConf(filePath, cfg)
	if err != nil {
		log.Fatal(err.Error())
	}

	return &Config{
		DB: &DBConfig{
			Dialect:  "postgres",
			Host:     cfg.DB_HOST,
			Username: cfg.DB_USER,
			Password: cfg.DB_PASSWORD,
			Name:     cfg.DB_NAME,
		},
		EthClient: &EthClientConf{
			NodeAddress:  cfg.NodeAddress,
			PathKeyStore: cfg.PathKeyStore,
		},
		IPFS: &IPFSConfig{
			Url: cfg.IPFS_ShellUrl,
		},
	}
}
