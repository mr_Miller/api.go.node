package model

import (
	"errors"
	"github.com/asaskevich/govalidator"
	"github.com/jinzhu/gorm"
)

func ValidateAnimalIPFS(animal *AnimalIPFS) (bool, error) {
	return govalidator.ValidateStruct(animal)
}

func ValidateUserCreate(user *UserInfo) (bool, error) {
	return govalidator.ValidateStruct(user)
}

func ValidateUser(user *Users) (bool, error) {
	if !govalidator.Matches(user.Phone, `^[+][0-9]{6,14}$`) {
		return false, errors.New("phone: invalid pattern +999999999999")
	}
	return govalidator.ValidateStruct(user)
}

func ValidateAuthToken(token string, db *gorm.DB) bool {
	var user Users
	if err := db.First(&user, "auth_key = ?", token).Error; err != nil {
		return false
	}
	return true
}
