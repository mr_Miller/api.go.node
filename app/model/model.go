package model

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"time"
)

type Model struct {
	ID        int       `gorm:"primary_key" json:"id"`
	CreatedAt time.Time `json:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
}

type ModelWithoutUpdated struct {
	ID        int       `gorm:"primary_key" json:"id"`
	CreatedAt time.Time `json:"created_at,omitempty"`
}

type ModelOnlyTime struct {
	CreatedAt time.Time `json:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
}

type Roles struct {
	Name string `gorm:"unique;size:100" json:"name"`
}

type Permissions struct {
	ID   int    `gorm:"primary_key" json:"id"`
	Name string `gorm:"size:100" json:"name"`
	Role string `gorm:"size:100" json:"role"`
}

type Countries struct {
	ID           int             `gorm:"unique" json:"id"`
	Alpha2       string          `gorm:"size:10" json:"alpha2,omitempty"`
	Alpha3       string          `gorm:"size:10" json:"alpha3,omitempty"`
	CountryNames []*CountryNames `gorm:"ForeignKey:CountryID;AssociationForeignKey:ID" json:"country_names,omitempty"`
}

type CountryNames struct {
	ID        int        `gorm:"primary_key" json:"id"`
	CountryID int        `json:"country_id"`
	Name      string     `gorm:"size:255" json:"name,omitempty"`
	Lang      string     `gorm:"size:20" json:"lang,omitempty"`
	Country   *Countries `gorm:"ForeignKey:ID;AssociationForeignKey:CountryID" json:"country,omitempty"`
}

type Users struct {
	Model
	Phone              string       `gorm:"size:100" json:"phone,omitempty" valid:"required"`
	Email              string       `gorm:"unique;size:100" json:"email,omitempty" valid:"required"`
	Password           string       `gorm:"size:255" json:"-" valid:"required"`
	VerifiedByAdmin    bool         `gorm:"default:false" json:"verified_by_admin,omitempty"`
	AuthKey            string       `gorm:"unique;size:255" json:"auth_key,omitempty"`
	PasswordResetToken string       `gorm:"size:255" json:"password_reset_token,omitempty"`
	Role               string       `gorm:"size:100" json:"role,omitempty" valid:"required"`
	Lang               string       `gorm:"size:10;default:'en'" json:"lang,omitempty"`
	UserInfo           *UserInfo    `gorm:"ForeignKey:ID;AssociationForeignKey:UserID" json:"user_info,omitempty"`
	Wallet             *Wallets     `gorm:"ForeignKey:ID;AssociationForeignKey:UserID" json:"wallet,omitempty"`
	Documents          []*Documents `gorm:"ForeignKey:UserID;AssociationForeignKey:ID" json:"documents,omitempty"`
}

func (u *Users) Verified() {
	u.VerifiedByAdmin = true
}

func (u *Users) Restore() {
	u.VerifiedByAdmin = false
}

type UserInfo struct {
	ID           int             `gorm:"primary_key" json:"id"`
	UserID       int             `json:"user_id"`
	Ownership    string          `gorm:"size:255" json:"ownership,omitempty" valid:"required"`
	Website      string          `gorm:"size:255" json:"website,omitempty" valid:"url"`
	Name         string          `gorm:"size:255" json:"name,omitempty" valid:"required,length(1|255)~name:MAX_LENGTH_255"`
	ShortDesc    string          `gorm:"size:300" json:"short_desc,omitempty" valid:"required,length(140|300)~short_desc:LENGTH_140_300"`
	Description  string          `json:"description,omitempty"`
	CountryISO   uint            `gorm:"default:null" sql:"country_iso" json:"country_iso,omitempty" valid:"required"`
	City         string          `gorm:"size:255" json:"city,omitempty" valid:"required"`
	Street       string          `gorm:"size:255" json:"street,omitempty" valid:"required"`
	HouseNumber  string          `gorm:"size:255" json:"house_number,omitempty" valid:"required"`
	StartTime    string          `gorm:"size:255" json:"start_time,omitempty" valid:"length(0|255)~start_time:MAX_LENGTH_255"`
	EndTime      string          `gorm:"size:255" json:"end_time,omitempty" valid:"length(0|255)~end_time:MAX_LENGTH_255"`
	WhyYou       string          `gorm:"size:500" json:"why_you,omitempty" valid:"length(0|500)~why_you: Max length 500"`
	Lat          string          `gorm:"size:255" json:"lat,omitempty" valid:"latitude"`
	Lng          string          `gorm:"size:255" json:"lng,omitempty" valid:"longitude"`
	Users        *Users          `gorm:"ForeignKey:UserID;AssociationForeignKey:ID" json:"user,omitempty"`
	Country      *Countries      `gorm:"ForeignKey:CountryISO;AssociationForeignKey:ID" json:"country,omitempty"`
	CountryNames []*CountryNames `gorm:"ForeignKey:CountryID;AssociationForeignKey:CountryISO" json:"countries,omitempty"`
}

type Documents struct {
	Model
	Name   string `gorm:"size:255" json:"name" valid:"required"`
	UserID int    `json:"user_id"`
	Users  *Users `gorm:"ForeignKey:UserID;AssociationForeignKey:ID" json:"user,omitempty"`
}

const TypeRegisterData = "register_data"   // register_data - реєстрування даних
const TypeMoneyTransfer = "money_transfer" // money_transfer- грошовий переказ

type Transactions struct {
	Txn             string `gorm:"unique;size:100" json:"txn"`
	BlockNumber     int64  `json:"block_number,omitempty"`
	From            string `gorm:"size:100" json:"from,omitempty"`
	To              string `gorm:"size:100" json:"to,omitempty"`
	Amount          string `gorm:"default:0" json:"amount,omitempty"`
	Data            string `json:"data,omitempty"`
	Type            string `gorm:"size:100" json:"type,omitempty"`
	IndexStatus     int    `gorm:"default:0" json:"index_status,omitempty"`
	TransactionDate int64  `json:"transaction_date,omitempty"`
	ModelOnlyTime
	Animals    []*Animals `gorm:"ForeignKey:TxnID;AssociationForeignKey:Txn" json:"animal_records,omitempty"`
	FromWallet *Wallets   `gorm:"ForeignKey:From;AssociationForeignKey:Address" json:"from_wallet,omitempty"`
	ToWallet   *Wallets   `gorm:"ForeignKey:To;AssociationForeignKey:Address" json:"to_wallet,omitempty"`
}

type Wallets struct {
	Model
	UserID   int    `json:"user_id,omitempty"`
	Address  string `gorm:"size:100" json:"address,omitempty"`
	Password string `gorm:"size:255" json:"password,omitempty"`
	Filename string `gorm:"size:255" json:"filename,omitempty"`
	Unlock   bool   `gorm:"default:false" json:"unlock,omitempty"`
	Users    *Users `gorm:"ForeignKey:UserID;AssociationForeignKey:ID" json:"user,omitempty"`
}

func (w *Wallets) UnlockWallet() {
	if w.Address != "" {
		w.Unlock = true
	}
}

type Transponders struct {
	Transponder       string     `gorm:"unique;size:20" json:"transponder"`
	ParentTransponder string     `gorm:"size:20" json:"parent_transponder"`
	RegisterDate      int64      `json:"register_date"`
	AnimalRecords     []*Animals `gorm:"ForeignKey:TransponderID;AssociationForeignKey:Transponder" json:"animal_records,omitempty"`
}

type AnimalIPFS struct {
	Transponder       string `json:"transponder" valid:"required,length(15|15)~transponder: length must 15 digital"`
	ItisTSN           int    `json:"itis_tsn,omitempty"`
	Breed             string `json:"breed,omitempty"`
	Gender            string `json:"gender,omitempty" valid:"in(female|male|unknown)"`
	Tag               string `json:"tag,omitempty"`
	Nickname          string `json:"nickname,omitempty"`
	Color             string `json:"color,omitempty"`
	BirthDate         int64  `json:"birth_date,omitempty"`
	DeathDate         int64  `json:"death_date,omitempty"`
	Sterilization     int64  `json:"sterilization,omitempty"`
	Vaccination       string `json:"vaccination,omitempty"`
	FatherTransponder string `json:"father_transponder,omitempty"`
	MotherTransponder string `json:"mother_transponder,omitempty"`
	Blood             string `json:"blood,omitempty"`
	AdvancedInfo      string `json:"advanced_info,omitempty"`
	RegisterDate      int64  `json:"register_date,omitempty"`
}

type AnimalsField struct {
	Title string `gorm:"unique;size:255" json:"title"`
	Type  string `gorm:"size:20" json:"type"`
}

type Animals struct {
	ID            int           `gorm:"primary_key" json:"id,omitempty"`
	TransponderID string        `gorm:"size:20" json:"transponder_id,omitempty"`
	FieldID       string        `json:"field_id,omitempty"`
	Value         string        `json:"value,omitempty"`
	TxnID         string        `gorm:"size:255" json:"txn_id,omitempty"`
	FileHash      string        `gorm:"size:255" json:"file_hash,omitempty"`
	CreatedAt     time.Time     `json:"created_at,omitempty"`
	Fields        *AnimalsField `gorm:"ForeignKey:FieldID;AssociationForeignKey:ID" json:"field,omitempty"`
	Transactions  *Transactions `gorm:"ForeignKey:ThxID;AssociationForeignKey:Thx" json:"transaction,omitempty"`
	Transponders  *Transponders `gorm:"ForeignKey:TransponderID;AssociationForeignKey:Transponder" json:"transponders,omitempty"`
}

type TranspondersWallets struct {
	ModelWithoutUpdated
	Transponder   string        `gorm:"size:20" json:"transponder"`
	Address       string        `gorm:"size:100" json:"address"`
	AnimalRecords []*Animals    `gorm:"ForeignKey:TransponderID;AssociationForeignKey:Transponder" json:"animal_records,omitempty"`
	Transponders  *Transponders `gorm:"ForeignKey:Transponder;AssociationForeignKey:Transponder" json:"transponders,omitempty"`
	Wallet        *Wallets      `grom:"ForeignKey:Address;AssociationForeignKey:Address" json:"wallet,omitempty"`
}

type Settings struct {
	ID     int    `gorm:"primary_key" json:"id"`
	Params string `json:"params"`
	Value  string `json:"value"`
}

type CallbackLogs struct {
	ModelWithoutUpdated
	Code     int    `json:"code,omitempty"`
	UserID   int    `json:"user_id,omitempty"`
	Data     string `json:"data,omitempty"`
	Response string `json:"response,omitempty"`
	Users    *Users `gorm:"ForeignKey:UserID;AssociationForeignKey:ID" json:"user,omitempty"`
}
