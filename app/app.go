package app

import (
	"api.go.node/app/model"
	"api.go.node/app/rbac"
	"api.go.node/config"
	"api.go.node/handler"
	"context"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/ipfs/go-ipfs-api"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/mikespook/gorbac"
	"log"
	"net/http"
	"strconv"
	"strings"
)

// App has router, db, eth client and new keystore instances
type App struct {
	Router       *mux.Router
	DB           *gorm.DB
	Rbac         *gorbac.RBAC
	Permissions  gorbac.Permissions
	EthClient    *ethclient.Client
	RpcClient    *rpc.Client
	NewKeyStore  *keystore.KeyStore
	PathKeyStore string
	IdentityUser *model.Users
	IPFSShell    *shell.Shell
}

// Initialize initializes the app with predefined configuration
func (a *App) Initialize(config *config.Config) {
	dbURI := fmt.Sprintf("host=%s port=5432 user=%s dbname=%s password=%s sslmode=disable",
		config.DB.Host,
		config.DB.Username,
		config.DB.Name,
		config.DB.Password,
	)
	db, err := gorm.Open(config.DB.Dialect, dbURI)
	if err != nil {
		log.Fatal("Could not connect database")
	}
	a.EthClient, err = ethclient.Dial(config.EthClient.NodeAddress)
	a.RpcClient, err = rpc.DialIPC(context.Background(), config.EthClient.NodeAddress)
	if err != nil {
		log.Fatal("Could not connect eth node")
	}
	a.PathKeyStore = config.EthClient.PathKeyStore
	a.NewKeyStore = keystore.NewKeyStore(config.EthClient.PathKeyStore, keystore.LightScryptN, keystore.LightScryptP)
	a.DB = db
	a.IPFSShell = shell.NewShell(config.IPFS.Url)
	a.Rbac, a.Permissions = rbac.InitRoles(db)
	a.Router = mux.NewRouter()
	a.setRouters()
}

// Get wraps the router for GET method
func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Post wraps the router for POST method
func (a *App) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Put wraps the router for PUT method
func (a *App) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Delete wraps the router for DELETE method
func (a *App) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

// setRouters sets the all required routers
func (a *App) setRouters() {
	// Indexer blockchain
	a.Get("/indexer", a.MasterIndexer)
	a.Get("/reserve", a.ReserveIndexer)

	// login
	a.Get("/login", a.Login)

	// countries
	a.Get("/countries", a.GetCountries)
	a.Get("/home/statistics", a.statisticsMap)

	// users
	a.Post("/users", a.CreateUser)
	a.Get("/users", a.authMiddleware(a.GetUsers, "GetUsers"))
	a.Get("/users/{id}", a.authMiddleware(a.DetailUserById, "DetailUserById"))
	a.Put("/users/{id}", a.authMiddleware(a.UpdateUser, "UpdateUser"))
	a.Put("/users/verified/{id}", a.authMiddleware(a.VerifiedUserById, "VerifiedUserById"))
	a.Put("/users/locked/{id}", a.authMiddleware(a.LockUserById, "LockUserById"))
	a.Get("/profile", a.authMiddleware(a.myProfile, "MyProfile"))
	a.Get("/profile/{address}", a.profileUserByAddress)

	// Wallet
	a.Get("/wallet", a.authMiddleware(a.myWallet, "MyWallet"))
	a.Get("/wallet/balance", a.authMiddleware(a.balancePTR, "MyBalance"))

	// users info / document
	a.Put("/user-info/{id}", a.authMiddleware(a.UpdateUserInfo, "UpdateUserInfo"))
	a.Post("/documents/{id}", a.authMiddleware(a.AddDocuments, "AddDocuments"))
	a.Delete("/documents/{id}", a.authMiddleware(a.RemoveDocument, "RemoveDocument"))
	a.Get("/documents/{id}", a.authMiddleware(a.GetDocument, "GetDocument"))

	// blockchain account
	a.Post("/account/create/{id}", a.authMiddleware(a.CreateAccount, "CreateAccount"))
	a.Put("/account/lock/{id}", a.authMiddleware(a.LockBcAccount, "LockBcAccount"))
	a.Put("/account/unlock/{id}", a.authMiddleware(a.UnLockBcAccount, "UnLockBcAccount"))
	a.Get("/account/balance/{address}", a.userBalancePTR)

	// blockchain transaction
	a.Post("/transactions/batch-animal", a.authMiddleware(a.sendBatchAnimals, "SendBatchAnimals"))
	a.Post("/transactions/animal", a.authMiddleware(a.sendAnimalTransaction, "SendAnimalTransaction"))
	a.Post("/transactions/coin", a.authMiddleware(a.sendCoinTransaction, "SendCoinTransaction"))
	//a.Get("/transactions/{address}", a.authMiddleware(a.GetTransactionsByAddress, "TransactionsByAddress"))

	// animal
	a.Get("/animals", a.authMiddleware(a.animalsByUser, "AnimalsByUser"))
	a.Get("/animals/{id}", a.animalProfile)

	// Explorer
	a.Get("/explorer", a.explorerBlocks)
	a.Get("/explorer/search", a.explorerSearch)
	a.Get("/explorer/{block}", a.explorerBlock)
	a.Get("/explorer/{block}/{tx}", a.explorerTransaction)
	//a.Get("/projects/{title}/tasks/{id:[0-9]+}", a.GetTask)
}

func (a *App) authMiddleware(next http.HandlerFunc, perm string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		auth := strings.SplitN(r.Header.Get("Authorization"), " ", 2)
		if len(auth) != 2 || auth[0] != "Bearer" {
			handler.HttpError(w, http.StatusUnauthorized, "Authorization failed")
			return
		}
		var user = &model.Users{}
		a.DB.Preload("Wallet", func(db *gorm.DB) *gorm.DB {
			return db.Select("user_id, address, unlock")
		}).Select("id, phone, email, verified_by_admin, auth_key, role, lang").
			Where("auth_key = ?", auth[1]).First(user)
		if user.ID != 0 {
			if user.VerifiedByAdmin {
				a.IdentityUser = user
				_rbac := a.Rbac
				value, isSet := a.Permissions[perm]
				if isSet && _rbac.IsGranted(user.Role, value, nil) {
					next.ServeHTTP(w, r)
				} else {
					handler.HttpError(w, http.StatusForbidden, "No access")
				}
			} else {
				handler.HttpError(w, http.StatusUnauthorized, "Your account is not verified")
			}
		} else {
			handler.HttpError(w, http.StatusUnauthorized, "Authorization failed")
		}
	})
}

func (a *App) MasterIndexer(w http.ResponseWriter, r *http.Request) {
	handler.Master(a.DB, a.EthClient, a.IPFSShell, w)
}

func (a *App) ReserveIndexer(w http.ResponseWriter, r *http.Request) {
	handler.Reserve(a.DB, a.EthClient, a.IPFSShell, w)
}

func (a *App) Login(w http.ResponseWriter, r *http.Request) {
	handler.BasicAuth(a.DB, w, r)
}

func (a *App) GetCountries(w http.ResponseWriter, r *http.Request) {
	handler.Countries(a.DB, w, r)
}

func (a *App) CreateUser(w http.ResponseWriter, r *http.Request) {
	handler.CreateUser(a.DB, w, r)
}

func (a *App) GetUsers(w http.ResponseWriter, r *http.Request) {
	handler.GetUsers(a.DB, w)
}

func (a *App) statisticsMap(w http.ResponseWriter, r *http.Request) {
	handler.GetStatisticsMap(a.DB, a.EthClient, w, r)
}

func (a *App) DetailUserById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId, _ := strconv.Atoi(vars["id"])
	handler.DetailUserById(a.DB, userId, w)
}

func (a *App) myProfile(w http.ResponseWriter, r *http.Request) {
	handler.DetailUserById(a.DB, a.IdentityUser.ID, w)
}

func (a *App) UpdateUser(w http.ResponseWriter, r *http.Request) {
	handler.UpdateUser(a.DB, w, r)
}

func (a *App) UpdateUserInfo(w http.ResponseWriter, r *http.Request) {
	handler.UpdateUserInfo(a.DB, w, r)
}

func (a *App) VerifiedUserById(w http.ResponseWriter, r *http.Request) {
	handler.VerifiedUserById(a.DB, a.NewKeyStore, w, r)
}

func (a *App) LockUserById(w http.ResponseWriter, r *http.Request) {
	handler.LockUserById(a.DB, a.IdentityUser, w, r)
}

func (a *App) AddDocuments(w http.ResponseWriter, r *http.Request) {
	handler.AddDocuments(a.DB, w, r)
}

func (a *App) RemoveDocument(w http.ResponseWriter, r *http.Request) {
	handler.RemoveDocument(a.DB, w, r)
}

func (a *App) GetDocument(w http.ResponseWriter, r *http.Request) {
	handler.GetDocument(a.DB, w, r)
}

func (a *App) CreateAccount(w http.ResponseWriter, r *http.Request) {
	handler.NewAccount(a.DB, a.NewKeyStore, w, r)
}

func (a *App) LockBcAccount(w http.ResponseWriter, r *http.Request) {
	handler.LockedBcAccount(a.NewKeyStore, w, r)
}

func (a *App) UnLockBcAccount(w http.ResponseWriter, r *http.Request) {
	handler.UnlockedBcAccount(a.NewKeyStore, w, r)
}

func (a *App) userBalancePTR(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	addr := common.HexToAddress(vars["address"])
	handler.BalanceAccount(a.EthClient, addr, w)
}

func (a *App) balancePTR(w http.ResponseWriter, r *http.Request) {
	var walletAddress string
	_ = a.DB.Table("wallets").Select("address").Where("user_id = ?", a.IdentityUser.ID).Row().Scan(&walletAddress)
	addr := common.HexToAddress(walletAddress)
	handler.BalanceAccount(a.EthClient, addr, w)
}

func (a *App) myWallet(w http.ResponseWriter, r *http.Request) {
	account := handler.FindAccount(a.DB, a.EthClient, a.IdentityUser)
	account.MyWallet(a.DB, w, r)
}

func (a *App) sendAnimalTransaction(w http.ResponseWriter, r *http.Request) {
	data := handler.TransactionData{DB: a.DB, Identity: a.IdentityUser, Client: a.EthClient,
		Shell: a.IPFSShell, PathKeyStore: a.PathKeyStore}
	data.Send(w, r)
}

func (a *App) sendBatchAnimals(w http.ResponseWriter, r *http.Request) {
	data := handler.TransactionData{DB: a.DB, Identity: a.IdentityUser, Client: a.EthClient,
		Shell: a.IPFSShell, PathKeyStore: a.PathKeyStore}
	data.SendAnimals(w, r)
}

func (a *App) sendCoinTransaction(w http.ResponseWriter, r *http.Request) {
	data := handler.TransactionCoin{DB: a.DB, Identity: a.IdentityUser, Client: a.EthClient,
		RPC: a.RpcClient, PathKeyStore: a.PathKeyStore}
	data.Send(w, r)
}

//func (a *App) GetTransactionsByAddress(w http.ResponseWriter, r *http.Request) {
//	handler.GetTransactionsByAddress(a.DB, w, r)
//}

func (a *App) animalProfile(w http.ResponseWriter, r *http.Request) {
	handler.GetAnimalProfile(a.DB, w, r)
}

func (a *App) explorerBlocks(w http.ResponseWriter, r *http.Request) {
	handler.ExplorerBlocks(a.EthClient, w, r)
}

func (a *App) explorerBlock(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	number, isHash := strconv.Atoi(vars["block"])
	if isHash != nil {
		if len(vars["block"]) > 0 {
			handler.BlockByHash(a.EthClient, vars["block"], w, r)
		} else {
			handler.HttpError(w, http.StatusBadRequest, "Bad request")
			return
		}
	}
	if number > 0 {
		handler.BlockByNumber(a.EthClient, number, w, r)
	} else {
		handler.HttpError(w, http.StatusBadRequest, "Bad request")
		return
	}
}

func (a *App) explorerTransaction(w http.ResponseWriter, r *http.Request) {
	handler.TransactionInfo(a.EthClient, w, r)
}

func (a *App) explorerSearch(w http.ResponseWriter, r *http.Request) {
	handler.ExplorerSearch(a.EthClient, a.RpcClient, w, r)
}

func (a *App) animalsByUser(w http.ResponseWriter, r *http.Request) {
	handler.GetAnimalsByUser(a.DB, a.IdentityUser, w, r)
}

func (a *App) profileUserByAddress(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	handler.PublicProfileUserByAddress(a.DB, vars["address"], w)
}

// Run the app on it's router
func (a *App) Run(host string) {
	allowedHeaders := handlers.AllowedHeaders([]string{
		"X-Requested-With", "Authorization", "Accept", "Content-Type", "Accept-Encoding",
	})
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE"})
	fmt.Println("Start Server", host)
	log.Fatal(http.ListenAndServe(host, handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods)(a.Router)))
}
