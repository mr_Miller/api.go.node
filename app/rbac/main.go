package rbac

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"api.go.node/app/model"
	"github.com/mikespook/gorbac"
)

func InitRoles(db *gorm.DB) (*gorbac.RBAC, gorbac.Permissions) {
	rbac := gorbac.New()
	permissions := make(gorbac.Permissions)
	roles := []model.Roles{}
	db.Find(&roles)

	for _, item := range roles {
		permissionsDb := []model.Permissions{}
		db.Where("role = ?", item.Name).Find(&permissionsDb)
		r := gorbac.NewStdRole(item.Name)
		if len(permissionsDb) > 0 {
			for _, p := range permissionsDb {
				_, ok := permissions[p.Name]
				if !ok {
					permissions[p.Name] = gorbac.NewStdPermission(p.Name)
				}
				r.Assign(permissions[p.Name])
			}
		}
		if err := rbac.Add(r); err != nil {
			fmt.Printf("Error: %s", err)
		}
	}
	return rbac, permissions
}
