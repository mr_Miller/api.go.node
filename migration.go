package main

import (
	"api.go.node/app/model"
	"api.go.node/config"
	"encoding/csv"
	"fmt"
	"github.com/jinzhu/gorm"
	"gopkg.in/gormigrate.v1"
	"os"
	"strconv"
)

func migrateInit(migrate []*gormigrate.Migration) *gormigrate.Gormigrate {
	configDB := config.GetConfig()
	dbURI := fmt.Sprintf("host=%s port=5432 user=%s dbname=%s password=%s sslmode=disable",
		configDB.DB.Host,
		configDB.DB.Username,
		configDB.DB.Name,
		configDB.DB.Password,
	)
	db, _ := gorm.Open(configDB.DB.Dialect, dbURI)
	db.LogMode(true)

	return gormigrate.New(db, gormigrate.DefaultOptions, migrate)
}

var m *gormigrate.Gormigrate

func init() {
	migrate := []*gormigrate.Migration{
		{
			ID: "init",
			Migrate: func(tx *gorm.DB) error {
				err := tx.AutoMigrate(&model.Roles{}, &model.Permissions{}, &model.Countries{}, &model.CountryNames{}, &model.Transactions{},
					&model.Users{}, &model.UserInfo{}, &model.Documents{}, &model.Wallets{}, &model.Transponders{},
					&model.AnimalsField{}, &model.Animals{}, &model.Settings{}, &model.CallbackLogs{}, &model.CallbackLogs{},
					&model.TranspondersWallets{}).Error
				tx.Model(&model.CountryNames{}).AddForeignKey("country_id", "countries(id)", "CASCADE", "CASCADE")
				tx.Model(&model.Users{}).AddForeignKey("role", "roles(name)", "CASCADE", "CASCADE")
				tx.Model(&model.UserInfo{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")
				tx.Model(&model.Documents{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")
				tx.Model(&model.Wallets{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")
				tx.Model(&model.Animals{}).AddForeignKey("field_id", "animals_fields(title)", "CASCADE", "CASCADE")
				tx.Model(&model.Animals{}).AddForeignKey("txn_id", "transactions(txn)", "CASCADE", "CASCADE")
				tx.Model(&model.Animals{}).AddForeignKey("transponder_id", "transponders(transponder)", "CASCADE", "CASCADE")
				tx.Model(&model.Permissions{}).AddForeignKey("role", "roles(name)", "CASCADE", "CASCADE")
				tx.Model(&model.CallbackLogs{}).AddForeignKey("user_id", "users(id)", "CASCADE", "CASCADE")

				tx.FirstOrCreate(&model.Roles{Name: "admin"}, "name = ?", "admin")
				tx.FirstOrCreate(&model.Roles{Name: "noda"}, "name = ?", "noda")
				tx.FirstOrCreate(&model.Roles{Name: "user"}, "name = ?", "user")

				tx.Create(&model.Permissions{Name: "GetUsers", Role: "admin"})
				tx.Create(&model.Permissions{Name: "DetailUserById", Role: "admin"})
				tx.Create(&model.Permissions{Name: "UpdateUser", Role: "admin"})
				tx.Create(&model.Permissions{Name: "VerifiedUserById", Role: "admin"})
				tx.Create(&model.Permissions{Name: "LockUserById", Role: "admin"})
				tx.Create(&model.Permissions{Name: "MyProfile", Role: "admin"})
				tx.Create(&model.Permissions{Name: "MyWallet", Role: "admin"})
				tx.Create(&model.Permissions{Name: "MyBalance", Role: "admin"})
				tx.Create(&model.Permissions{Name: "UpdateUserInfo", Role: "admin"})
				tx.Create(&model.Permissions{Name: "AddDocuments", Role: "admin"})
				tx.Create(&model.Permissions{Name: "RemoveDocument", Role: "admin"})
				tx.Create(&model.Permissions{Name: "GetDocument", Role: "admin"})
				tx.Create(&model.Permissions{Name: "CreateAccount", Role: "admin"})
				tx.Create(&model.Permissions{Name: "LockBcAccount", Role: "admin"})
				tx.Create(&model.Permissions{Name: "UnLockBcAccount", Role: "admin"})
				tx.Create(&model.Permissions{Name: "SendBatchAnimals", Role: "admin"})
				tx.Create(&model.Permissions{Name: "SendAnimalTransaction", Role: "admin"})
				tx.Create(&model.Permissions{Name: "SendCoinTransaction", Role: "admin"})
				tx.Create(&model.Permissions{Name: "TransactionsByAddress", Role: "admin"})
				tx.Create(&model.Permissions{Name: "AnimalsByUser", Role: "admin"})
				tx.Create(&model.Permissions{Name: "MyProfile", Role: "user"})
				tx.Create(&model.Permissions{Name: "MyWallet", Role: "user"})
				tx.Create(&model.Permissions{Name: "MyBalance", Role: "user"})
				tx.Create(&model.Permissions{Name: "GetDocument", Role: "user"})
				tx.Create(&model.Permissions{Name: "SendBatchAnimals", Role: "user"})
				tx.Create(&model.Permissions{Name: "SendAnimalTransaction", Role: "user"})
				tx.Create(&model.Permissions{Name: "SendCoinTransaction", Role: "user"})
				tx.Create(&model.Permissions{Name: "AnimalsByUser", Role: "user"})
				tx.Create(&model.Permissions{Name: "MyProfile", Role: "noda"})
				tx.Create(&model.Permissions{Name: "MyWallet", Role: "noda"})
				tx.Create(&model.Permissions{Name: "MyBalance", Role: "noda"})
				tx.Create(&model.Permissions{Name: "GetDocument", Role: "noda"})
				tx.Create(&model.Permissions{Name: "SendBatchAnimals", Role: "noda"})
				tx.Create(&model.Permissions{Name: "SendAnimalTransaction", Role: "noda"})
				tx.Create(&model.Permissions{Name: "SendCoinTransaction", Role: "noda"})
				tx.Create(&model.Permissions{Name: "AnimalsByUser", Role: "noda"})

				tx.FirstOrCreate(&model.Users{
					Phone:              "+380991112233",
					Email:              "admin@animal.id",
					Password:           "$2y$13$IuuZHITie.DtEEYi8RCVFu/yU6Z8MzKm5fu1nyK7uWnV7zCi9yfVC",
					AuthKey:            "uIdKdxY52qgDbHyZ1CEH7akBDdik6IYX",
					PasswordResetToken: "reset",
					VerifiedByAdmin:    true,
					Role:               "admin",
					Lang:               "en",
				}, "email = ?", "admin@animal.id")
				return err
			},
			Rollback: func(tx *gorm.DB) error {
				tx.Model(&model.CountryNames{}).RemoveForeignKey("country_id", "countries(id)")
				tx.Model(&model.Users{}).RemoveForeignKey("role", "roles(name)")
				tx.Model(&model.UserInfo{}).RemoveForeignKey("user_id", "users(id)")
				tx.Model(&model.Documents{}).RemoveForeignKey("user_id", "users(id)")
				tx.Model(&model.Wallets{}).RemoveForeignKey("user_id", "users(id)")
				tx.Model(&model.Animals{}).RemoveForeignKey("transponder", "transponders(transponder)")
				tx.Model(&model.Animals{}).RemoveForeignKey("txn_id", "transactions(txn)")
				tx.Model(&model.Animals{}).RemoveForeignKey("field_id", "animals_fields(id)")
				tx.Model(&model.Permissions{}).RemoveForeignKey("role", "roles(name)")
				tx.Model(&model.CallbackLogs{}).RemoveForeignKey("user_id", "users(id)")
				err := tx.DropTable(&model.Transactions{}, &model.UserInfo{}, &model.Documents{}, &model.Wallets{},
					&model.Users{}, &model.Roles{}, &model.Permissions{}, &model.CountryNames{}, &model.Countries{},
					&model.AnimalsField{}, &model.Animals{}, &model.Settings{}, &model.Transponders{}, &model.CallbackLogs{},
					&model.TranspondersWallets{}).Error
				return err
			},
		},
		{
			ID: "add_animals_field",
			Migrate: func(tx *gorm.DB) error {
				tx.FirstOrCreate(&model.AnimalsField{Title: "itis_tsn", Type: "int"}, "title = ?", "itis_tsn")
				tx.FirstOrCreate(&model.AnimalsField{Title: "breed", Type: "string"}, "title = ?", "breed")
				tx.FirstOrCreate(&model.AnimalsField{Title: "gender", Type: "string"}, "title = ?", "gender")
				tx.FirstOrCreate(&model.AnimalsField{Title: "tag", Type: "string"}, "title = ?", "tag")
				tx.FirstOrCreate(&model.AnimalsField{Title: "nickname", Type: "string"}, "title = ?", "nickname")
				tx.FirstOrCreate(&model.AnimalsField{Title: "color", Type: "string"}, "title = ?", "color")
				tx.FirstOrCreate(&model.AnimalsField{Title: "birth_date", Type: "int"}, "title = ?", "birth_date")
				tx.FirstOrCreate(&model.AnimalsField{Title: "death_date", Type: "int"}, "title = ?", "death_date")
				tx.FirstOrCreate(&model.AnimalsField{Title: "sterilization", Type: "int"}, "title = ?", "sterilization")
				tx.FirstOrCreate(&model.AnimalsField{Title: "vaccination", Type: "string"}, "title = ?", "vaccination")
				tx.FirstOrCreate(&model.AnimalsField{Title: "father_transponder", Type: "string"}, "title = ?", "father_transponder")
				tx.FirstOrCreate(&model.AnimalsField{Title: "mother_transponder", Type: "string"}, "title = ?", "mother_transponder")
				tx.FirstOrCreate(&model.AnimalsField{Title: "blood", Type: "string"}, "title = ?", "blood")
				tx.FirstOrCreate(&model.AnimalsField{Title: "advanced_info", Type: "string"}, "title = ?", "advanced_info")
				return tx.Error
			},
			Rollback: func(tx *gorm.DB) error {
				tx.Delete(&model.AnimalsField{})
				return tx.Error
			},
		},
		{
			ID: "add_countries_and_country_names",
			Migrate: func(tx *gorm.DB) error {
				f, err := os.Open("countries.csv")
				if err != nil {
					panic(err)
				}
				defer f.Close()
				lines, err := csv.NewReader(f).ReadAll()
				if err != nil {
					panic(err)
				}
				for _, line := range lines {
					iso, _ := strconv.Atoi(line[0])
					tx.FirstOrCreate(&model.Countries{ID: iso, Alpha2: line[1], Alpha3: line[2]}, "id = ?", iso)
					tx.FirstOrCreate(&model.CountryNames{CountryID: iso, Name: line[3], Lang: "uk"}, "name = ? AND lang = ?", line[3], "uk")
					tx.FirstOrCreate(&model.CountryNames{CountryID: iso, Name: line[4], Lang: "ru"}, "name = ? AND lang = ?", line[4], "ru")
					tx.FirstOrCreate(&model.CountryNames{CountryID: iso, Name: line[5], Lang: "en"}, "name = ? AND lang = ?", line[5], "en")
				}
				return tx.Error
			},
			Rollback: func(tx *gorm.DB) error {
				tx.Delete(&model.CountryNames{})
				tx.Delete(&model.Countries{})
				return tx.Error
			},
		},
		{
			ID: "create_view_animals",
			Migrate: func(db *gorm.DB) error {
				db.Exec("CREATE VIEW view_animals AS SELECT * " +
					"FROM crosstab($q$ SELECT transponder_id, field_id, value " +
					"FROM (SELECT txn, \"from\", transaction_date, transponder_id, field_id, value FROM transactions AS t " +
					"INNER JOIN animals AS a ON a.txn_id = t.txn WHERE t.index_status = 1 AND t.type = 'register_data' " +
					"ORDER BY transaction_date) AS animal_t " +
					"ORDER BY transponder_id, transaction_date $q$, " +
					"$q$ values ('advanced_info'), ('birth_date'), ('blood'), ('breed'), ('color'), ('death_date'), " +
					"('father_transponder'), ('gender'), ('itis_tsn'), ('mother_transponder'), ('nickname'), " +
					"('sterilization'), ('tag'), ('vaccination') $q$) " +
					"AS final_result(transponder_id character varying(20), advanced_info TEXT, birth_date TEXT, " +
					"blood TEXT, breed TEXT, color TEXT, death_date TEXT, father_transponder TEXT, gender TEXT, " +
					"itis_tsn TEXT, mother_transponder TEXT, nickname TEXT, sterilization TEXT, tag TEXT, vaccination TEXT) " +
					"ORDER BY transponder_id")
				return db.Error
			},
			Rollback: func(db *gorm.DB) error {
				db.Exec("DROP VIEW IF EXISTS view_animals")
				return db.Error
			},
		},
	}
	m = migrateInit(migrate)
}

func main() {
	if len(os.Args) >= 2 {
		switch os.Args[1] {
		case "up":
		default:
			m.Migrate()
			break
		case "down":
			m.RollbackLast()
			break
		}
	} else {
		m.Migrate()
	}
}
