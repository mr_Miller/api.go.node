Add file config.json
```
{
  "NodeAddress": "http://127.0.0.1:8545",
  "PathKeyStore": "/path/to/node/keystore/",
  "IPFS_ShellUrl": "127.0.0.1:5001",
  "DB_HOST": "127.0.0.1",
  "DB_NAME": "blockchain",
  "DB_USER": "root",
  "DB_PASSWORD": "root"
}
```

Run migration cmd "go run migration.go"